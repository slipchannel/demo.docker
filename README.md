# Slipchannel Demo (Docker)

This is a demo of how the Slipchannel service works, using Docker
for simplicity and ease of getting started.

## Features

* starts an SSH service
* has the latest slipchannel binary installed
* has a user that can be sshed into
* is a small image size
* is fast to run
* prints a message to stout once you are logged in

