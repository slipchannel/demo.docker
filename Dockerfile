FROM alpine:3.4
MAINTAINER Slipchannel

RUN apk add --update openssh curl

RUN ssh-keygen -A

RUN adduser -h /home/demo -s /bin/ash -G users -D demo
RUN echo 'demo:password' | chpasswd

COPY src/sshd_config /etc/ssh/sshd_config
COPY src/banner /etc/ssh/banner
COPY src/slipchannel /usr/local/bin/slipchannel
COPY src/motd /etc/motd
COPY src/profile /home/demo/.profile

RUN chmod 755 /usr/local/bin/slipchannel

EXPOSE 22

CMD ["/usr/sbin/sshd", "-D"]

