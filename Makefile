.DEFAULT_GOAL := all
.PHONY: help build start-server stop-server

all: help ## [DEFAULT] Display help

help: ## Displays this message
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## Builds a	new docker image called 'demo'
	docker build -t slipchannel_demo .

start-server: ## Starts the Docker container locally for testing
	docker run --detach --name=slipchannel_demo -p 2222:22 slipchannel_demo

stop-server: ## Stops and removes the test Docker container
	docker stop slipchannel_demo
	docker rm slipchannel_demo

